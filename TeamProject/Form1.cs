﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace TeamProject
{
	public partial class Form1 : Form
	{
		
		Point startPos, endPos, oldPos;
		Pallet pallet;
		int type;
		Color color;
		int penSize;
		Bitmap bitmap = null;
		Image img;

		public Form1()
		{
			
			InitializeComponent();
			this.pallet = new Pallet();
			pallet.Show();
			bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
			color = this.pallet.GetColor();
			this.endPos = new Point(-5000, 0);
			this.startPos = new Point(-5000, 0);
		}

		private void DrawFigures(object sender, PaintEventArgs e)
		{
			e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
			type = this.pallet.GetFigureType();
			color = this.pallet.GetColor();
			penSize = this.pallet.GetPenSize();
			img = this.pallet.GetStampImage();
			Graphics g = e.Graphics;
			if (type == 1)
			{
				EllipseDraw(g);
			}
			else if (type == 2)
			{
				RectangleDraw(g);
			}
			else if (type == 3)
			{
				LineDraw(g);
			}
			else if (type == 4)
			{
				using (Graphics g2 = Graphics.FromImage(bitmap))
				{
					g2.SmoothingMode = SmoothingMode.AntiAlias;
					SolidBrush brush = new SolidBrush(color);
					Pen p = new Pen(color, penSize);
					g2.FillEllipse(brush, this.endPos.X - p.Width/2, this.endPos.Y - p.Width / 2, p.Width, p.Width);
					g2.DrawLine(p, oldPos, endPos);
					oldPos = endPos;
				}
			}
			

		
			pictureBox1.Image = bitmap;
			
			
		}

		private void MouseDragged(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				this.endPos = new Point(e.X, e.Y);
				Invalidate();
			}
		}


		private void MouseUp(object sender, MouseEventArgs e)
		{
			
			using (Graphics g = Graphics.FromImage(bitmap))
			{
				g.SmoothingMode = SmoothingMode.AntiAlias;
				switch (type)
				{
					case 1:
						EllipseDraw(g);
						break;
					case 2:
						RectangleDraw(g);
						break;
					case 3:
						LineDraw(g);
						break;
					case 5:
						StampDraw(g);
						break;



				}
				this.endPos = new Point(-5000, 0);
				this.startPos = new Point(-5000, 0);
				this.oldPos = this.endPos;

			}
			//pictureBox1.Image = bitmap;
		}

		private void MousePressed(object sender, MouseEventArgs e)
		{
			this.endPos = new Point(e.X, e.Y);
			this.startPos = new Point(e.X, e.Y);
			this.oldPos = this.endPos;
		}
		private void LineDraw(Graphics g)
		{
			Pen p = new Pen(color, penSize);
			g.DrawLine(p, this.startPos.X, this.startPos.Y, this.endPos.X, this.endPos.Y);

		}

		private void RectangleDraw(Graphics g)
		{
			SolidBrush brush = new SolidBrush(color);
			int width = this.endPos.X - this.startPos.X;
			int height = this.endPos.Y - this.startPos.Y;
			g.FillRectangle(brush, this.startPos.X, this.startPos.Y, width, height);
		}

		private void AllClear(object sender, EventArgs e)
		{
			bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
		}

		private void StampDraw(Graphics g)
		{
			int width = this.endPos.X -(img.Width/2);
			int height = this.endPos.Y - (img.Height/2);
			g.DrawImage(img, width, height);
			this.pallet.StampBgm();
		}

		private void EllipseDraw(Graphics g)
		{
			SolidBrush brush = new SolidBrush(color);

			int width = this.endPos.X - this.startPos.X;
			int height = this.endPos.Y - this.startPos.Y;
			g.FillEllipse(brush, this.startPos.X, this.startPos.Y, width, height);
		}
	}
}
