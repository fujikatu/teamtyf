﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeamProject
{
    public partial class Pallet : Form
    {
        int figureType;
        int StampNum;

        public System.Media.SoundPlayer player = null;




        public int GetFigureType()
        {
            return figureType;
        }

        public Image GetStampImage()
        {
            Image img;

            switch (StampNum)
            {
                case 1:
                    img = Image.FromFile(@"..\..\image\main@2x.png");
                    break;

                case 2:
                    img = Image.FromFile(@"..\..\image\stamp2.png");
                    break;

                case 3:
                    img = Image.FromFile(@"..\..\image\stamp3.png");
                    break;
                case 4:
                    img = Image.FromFile(@"..\..\image\otikomi.png");
                    break;
                case 5:
                    img = Image.FromFile(@"..\..\image\stamp5.png");
                    break;


                default:
                    img = null;
                    break;

            }
            return img;

        }






        public int GetPenSize()
        {
            int size;
            if (int.TryParse(this.penSizeBox.Text, out size))
            {
                return size;
            }
            else
            {
                return 1;
            }
        }




        public Color GetColor()
        {
            return colorButton.BackColor;
        }



        public Pallet()
        {
            InitializeComponent();
            this.figureType = 1;
        }

        private void CircleButtonClicked(object sender, EventArgs e)
        {
            this.figureType = 1;
        }

        private void RectButtonClicked(object sender, EventArgs e)
        {
            this.figureType = 2;
        }

        private void LineButtonClicked(object sender, EventArgs e)
        {
            this.figureType = 3;
        }

        private void penChanged(object sender, EventArgs e)
        {
            this.figureType = 4;
        }


        //◆◆◆◆◆◆◆◆◆◆◆◆スタンプ◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆
        private void stampChange1(object sender, EventArgs e)
        {
            this.figureType = 5;
            StampNum = 1;

            

        }

        private void stampChange2(object sender, EventArgs e)
        {
            this.figureType = 5;
            StampNum = 2;
        }

        private void stampChange3(object sender, EventArgs e)
        {
            this.figureType = 5;
            StampNum = 3;
        }

        private void stampChange4(object sender, EventArgs e)
        {
            this.figureType = 5;
            StampNum = 4;
        }

        private void stampChange5(object sender, EventArgs e)
        {
            this.figureType = 5;
            StampNum = 5;
        }


        //◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆◆




        private void ColorButtonClicked(object sender, EventArgs e)
        {

        }



        public void StampBgm()
        {
            switch (StampNum)
            {
                case 1:
                    PlaySound(@"..\..\BGM\syakin.wav");
                    break;
                case 2:
                    PlaySound(@"..\..\BGM\punch.wav");
                    break;
                case 3:
                    PlaySound(@"..\..\BGM\bakuhatu.wav");
                    break;
                case 4:
                    PlaySound(@"..\..\BGM\tin.wav");
                    break;
                case 5:
                    PlaySound(@"..\..\BGM\dodon.wav");
                    break;


            }
            
            System.Media.SystemSounds.Beep.Play();
        }

        private void PlaySound(string waveFile)
        {

            //読み込む
            player = new System.Media.SoundPlayer(waveFile);
            //非同期再生する
            player.Play();

        }


        //ペン色
        private void ColorButtonClicked1(object sender, EventArgs e)
        {
            colorButton.BackColor = Kuro.BackColor;
        }

        private void ColorButtonClicked2(object sender, EventArgs e)
        {
            colorButton.BackColor = Aka.BackColor;
        }

        private void ColorButtonClicked3(object sender, EventArgs e)
        {
            colorButton.BackColor = Ki.BackColor;
        }

        private void ColorButtonClicked4(object sender, EventArgs e)
        {
            colorButton.BackColor = Ao.BackColor;
        }

        private void ColorButtonClicked5(object sender, EventArgs e)
        {
            colorButton.BackColor = Siro.BackColor;
        }
    }//class
}//namespace TeamProject

