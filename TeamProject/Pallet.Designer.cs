﻿namespace TeamProject
{
    partial class Pallet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.colorButton = new System.Windows.Forms.Button();
            this.penSizeBox = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Stamp1 = new System.Windows.Forms.Button();
            this.lineButton = new System.Windows.Forms.Button();
            this.rectangleButton = new System.Windows.Forms.Button();
            this.circleButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Aka = new System.Windows.Forms.Button();
            this.Ki = new System.Windows.Forms.Button();
            this.Ao = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Kuro = new System.Windows.Forms.Button();
            this.Siro = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // colorButton
            // 
            this.colorButton.BackColor = System.Drawing.SystemColors.WindowText;
            this.colorButton.Location = new System.Drawing.Point(381, 47);
            this.colorButton.Name = "colorButton";
            this.colorButton.Size = new System.Drawing.Size(59, 53);
            this.colorButton.TabIndex = 3;
            this.colorButton.UseVisualStyleBackColor = false;
            // 
            // penSizeBox
            // 
            this.penSizeBox.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.penSizeBox.Location = new System.Drawing.Point(66, 101);
            this.penSizeBox.Name = "penSizeBox";
            this.penSizeBox.Size = new System.Drawing.Size(100, 23);
            this.penSizeBox.TabIndex = 4;
            this.penSizeBox.Text = "3";
            // 
            // button5
            // 
            this.button5.BackgroundImage = global::TeamProject.Properties.Resources.stamp5;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button5.Location = new System.Drawing.Point(109, 254);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 67);
            this.button5.TabIndex = 11;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.stampChange5);
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::TeamProject.Properties.Resources.otikomi;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button4.Location = new System.Drawing.Point(28, 254);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 67);
            this.button4.TabIndex = 10;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.stampChange4);
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::TeamProject.Properties.Resources.stamp3;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button3.Location = new System.Drawing.Point(190, 181);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 67);
            this.button3.TabIndex = 9;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.stampChange3);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::TeamProject.Properties.Resources.stamp2;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button2.Location = new System.Drawing.Point(109, 181);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 67);
            this.button2.TabIndex = 8;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.stampChange2);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::TeamProject.Properties.Resources.pen;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button1.Location = new System.Drawing.Point(28, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 53);
            this.button1.TabIndex = 7;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.penChanged);
            // 
            // Stamp1
            // 
            this.Stamp1.BackgroundImage = global::TeamProject.Properties.Resources.main_2x;
            this.Stamp1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Stamp1.Location = new System.Drawing.Point(28, 181);
            this.Stamp1.Name = "Stamp1";
            this.Stamp1.Size = new System.Drawing.Size(75, 67);
            this.Stamp1.TabIndex = 6;
            this.Stamp1.UseVisualStyleBackColor = true;
            this.Stamp1.Click += new System.EventHandler(this.stampChange1);
            // 
            // lineButton
            // 
            this.lineButton.BackgroundImage = global::TeamProject.Properties.Resources.line;
            this.lineButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.lineButton.Location = new System.Drawing.Point(212, 26);
            this.lineButton.Name = "lineButton";
            this.lineButton.Size = new System.Drawing.Size(54, 53);
            this.lineButton.TabIndex = 2;
            this.lineButton.UseVisualStyleBackColor = true;
            this.lineButton.Click += new System.EventHandler(this.LineButtonClicked);
            // 
            // rectangleButton
            // 
            this.rectangleButton.BackgroundImage = global::TeamProject.Properties.Resources.sikaku;
            this.rectangleButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.rectangleButton.Location = new System.Drawing.Point(151, 26);
            this.rectangleButton.Name = "rectangleButton";
            this.rectangleButton.Size = new System.Drawing.Size(55, 53);
            this.rectangleButton.TabIndex = 1;
            this.rectangleButton.UseVisualStyleBackColor = true;
            this.rectangleButton.Click += new System.EventHandler(this.RectButtonClicked);
            // 
            // circleButton
            // 
            this.circleButton.BackgroundImage = global::TeamProject.Properties.Resources.maru;
            this.circleButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.circleButton.Location = new System.Drawing.Point(91, 26);
            this.circleButton.Name = "circleButton";
            this.circleButton.Size = new System.Drawing.Size(54, 53);
            this.circleButton.TabIndex = 0;
            this.circleButton.UseVisualStyleBackColor = true;
            this.circleButton.Click += new System.EventHandler(this.CircleButtonClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(302, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 12;
            this.label1.Text = "現在の色";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(26, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "太さ";
            // 
            // Aka
            // 
            this.Aka.BackColor = System.Drawing.Color.Red;
            this.Aka.Location = new System.Drawing.Point(335, 147);
            this.Aka.Name = "Aka";
            this.Aka.Size = new System.Drawing.Size(40, 36);
            this.Aka.TabIndex = 14;
            this.Aka.UseVisualStyleBackColor = false;
            this.Aka.Click += new System.EventHandler(this.ColorButtonClicked2);
            // 
            // Ki
            // 
            this.Ki.BackColor = System.Drawing.Color.Yellow;
            this.Ki.Location = new System.Drawing.Point(381, 147);
            this.Ki.Name = "Ki";
            this.Ki.Size = new System.Drawing.Size(40, 36);
            this.Ki.TabIndex = 15;
            this.Ki.UseVisualStyleBackColor = false;
            this.Ki.Click += new System.EventHandler(this.ColorButtonClicked3);
            // 
            // Ao
            // 
            this.Ao.BackColor = System.Drawing.Color.Blue;
            this.Ao.Location = new System.Drawing.Point(427, 148);
            this.Ao.Name = "Ao";
            this.Ao.Size = new System.Drawing.Size(39, 35);
            this.Ao.TabIndex = 16;
            this.Ao.UseVisualStyleBackColor = false;
            this.Ao.Click += new System.EventHandler(this.ColorButtonClicked4);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(118, 162);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "スタンプ";
            // 
            // Kuro
            // 
            this.Kuro.BackColor = System.Drawing.SystemColors.WindowText;
            this.Kuro.Location = new System.Drawing.Point(335, 106);
            this.Kuro.Name = "Kuro";
            this.Kuro.Size = new System.Drawing.Size(40, 35);
            this.Kuro.TabIndex = 18;
            this.Kuro.UseVisualStyleBackColor = false;
            this.Kuro.Click += new System.EventHandler(this.ColorButtonClicked1);
            // 
            // Siro
            // 
            this.Siro.BackColor = System.Drawing.SystemColors.Window;
            this.Siro.Location = new System.Drawing.Point(381, 106);
            this.Siro.Name = "Siro";
            this.Siro.Size = new System.Drawing.Size(40, 35);
            this.Siro.TabIndex = 19;
            this.Siro.UseVisualStyleBackColor = false;
            this.Siro.Click += new System.EventHandler(this.ColorButtonClicked5);
            // 
            // Pallet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(485, 344);
            this.Controls.Add(this.Siro);
            this.Controls.Add(this.Kuro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Ao);
            this.Controls.Add(this.Ki);
            this.Controls.Add(this.Aka);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Stamp1);
            this.Controls.Add(this.penSizeBox);
            this.Controls.Add(this.colorButton);
            this.Controls.Add(this.lineButton);
            this.Controls.Add(this.rectangleButton);
            this.Controls.Add(this.circleButton);
            this.Name = "Pallet";
            this.Text = "Pallet";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button circleButton;
        private System.Windows.Forms.Button rectangleButton;
        private System.Windows.Forms.Button lineButton;
        private System.Windows.Forms.Button colorButton;
        private System.Windows.Forms.TextBox penSizeBox;
        private System.Windows.Forms.Button Stamp1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Aka;
        private System.Windows.Forms.Button Ki;
        private System.Windows.Forms.Button Ao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Kuro;
        private System.Windows.Forms.Button Siro;
    }
}